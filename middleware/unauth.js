export default function ({ store, error, redirect }) {
  if (store.getters.getLogin) {
    return redirect('/')
  }
}
