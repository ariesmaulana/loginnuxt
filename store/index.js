export const state = () => ({
  token: null,
  login: false
})


// getter
export const getters = {
  getToken (state) {
    return state.token
  },
  getLogin (state) {
    return state.login
  }
}

export const mutations = {
  setLogin (state, data) {
    state.token = data.token
    state.login = data.login
  }
}


// actions
export const actions = {

  async nuxtServerInit ({ commit }, { req }) {
    if (req.session && req.session.userToken) {
      const data = {
        token: req.session.userToken,
        login: true
      }
      commit('setLogin', data)
    }
  }
}
