
const express = require('express')
const consola = require('consola')
// menambahkan body parser, express sesion, dan axios
const bodyParser = require('body-parser')
const session = require('express-session')
const axios = require('axios')
const { Nuxt, Builder } = require('nuxt')
const app = express()
const host = process.env.HOST || '127.0.0.1'
const port = process.env.PORT || 3000

app.set('port', port)

// agar bisa menammngkap req body dari form
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(bodyParser.json())

// Sessions untuk membentuk `req.session`
app.use(session({
  secret: 'super-secret-key',
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 60000 }
}))


// Import and Set Nuxt.js options
let config = require('../nuxt.config.js')
config.dev = !(process.env.NODE_ENV === 'production')

async function start() {
  // Init Nuxt.js
  const nuxt = new Nuxt(config)

  // Build only in dev mode
  if (config.dev) {
    const builder = new Builder(nuxt)
    await builder.build()
  }

  // Give nuxt middleware to express
  app.use(nuxt.render)

  // Listen the server
  app.listen(port, host)
  consola.ready({
    message: `Server listening on http://${host}:${port}`,
    badge: true
  })
}
start()

app.post('/login', function (req, res) {
  const body = {
    email: req.body.email,
    password: req.body.password
  }
  // res.json(body)
  axios.post('https://reqres.in/api/login', body)
    .then((response) => {
      req.session.userToken = response.data.token
      res.redirect('http://localhost:3000/secret')
    })
    .catch((error) => {
      console.log('caught', error.message)
      res.redirect('http://localhost:3000/login?info=periksa kembali email dan password anda')
    })
})

app.post('/logout', function (req, res) {
  delete req.session.userToken
  res.json({ ok: true })
  res.send()
})
